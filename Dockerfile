FROM tensorflow/tensorflow

RUN apt-get update

RUN apt-get install ffmpeg libsm6 libxext6  -y


WORKDIR /undeep_vo

COPY ./requirements.txt .

RUN pip install -r requirements.txt

LABEL MAINTAINER="Mohammad Reza Baqery"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . .


CMD ["/bin/bash"]