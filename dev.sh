#!/bin/bash

bash() {
  docker container exec -it undeepvo_app_1 /bin/bash
}

pytest() {
  python -m unittest discover
}

run_ngrok() {
  ngrok authtoken 22spKEx9dHpf5fe3d7pnKPkWjuK_5ab5LfL2K5ym2xjCDzH5x
  echo "$(date) : ngrok authtoken is set"
  killall ngrok
  echo "$(date) : all open ngrok tunnel is killed"
  nohup python ngrok.py &
  echo "$(date) : ngrok.py is started in background"
  cat ngrok.py
}

case "$1" in
bash)
  bash
  ;;
test)
  pytest
  ;;
run_ngrok)
  run_ngrok
  ;;

\
  *)
  echo >$2 unsupported command: $1
  exit 1
  ;;
esac
