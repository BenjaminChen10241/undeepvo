import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models

from torchsummary import summary

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class SingleConv(nn.Module):
    """(convolution => [BN] => ReLU)"""

    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        """
        :param x: input tensor from previous layer
        :return: tensor after applying computation
        """
        return self.double_conv(x)


class Down(nn.Module):

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            SingleConv(in_channels, out_channels)
        )

    def forward(self, x):
        """
        :param x: input tensor from previous layer
        :return: tensor after applying computation
        """
        return self.maxpool_conv(x)


class Up(nn.Module):
    """
    Upscaling then double conv
    """

    def __init__(self, in_channels, out_channels, bilinear=False):
        super().__init__()

        self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.conv = SingleConv(in_channels, out_channels)

    def forward(self, x1, x2):
        """
        :param x1: input tensor from previous layer
        :param x2: input tensor from prev skip layer
        :return: tensor after applying computation
        """

        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2, diffY // 2, diffY - diffY // 2])
        x = torch.cat([x1, x2], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        """
        :param x: input tensor from previous layer
        :return: tensor after applying computation
        """
        return self.conv(x)


class ResNetUNet(nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=False, min_depth=1, max_depth=100):
        super().__init__()

        self.n_channels = n_channels
        self.n_classes = n_classes
        self.bilinear = bilinear
        self.min_depth = min_depth
        self.max_depth = max_depth

        self.base_model = torchvision.models.resnet18(pretrained=True)
        self.base_layers = list(self.base_model.children())

        self.input1 = SingleConv(n_channels, 64)
        self.input2 = SingleConv(64, 64)

        self.down1 = nn.Sequential(*self.base_layers[:3])  # size=(N, 64, x.H/2, x.W/2)
        self.down2 = nn.Sequential(*self.base_layers[3:5])  # size=(N, 64, x.H/4, x.W/4)
        self.down3 = self.base_layers[5]  # size=(N, 128, x.H/8, x.W/8)
        self.down4 = self.base_layers[6]  # size=(N, 256, x.H/16, x.W/16)
        self.down5 = self.base_layers[7]  # size=(N, 512, x.H/32, x.W/32)

        self.up4 = Up(256 + 512, 512, self.bilinear)
        self.up3 = Up(128 + 512, 256, self.bilinear)
        self.up2 = Up(64 + 256, 256, self.bilinear)
        self.up1 = Up(64 + 256, 128, self.bilinear)
        self.up0 = Up(64 + 128, 64, self.bilinear)

        self.output = OutConv(64, n_classes)

    def forward(self, x):
        x0 = self.input1(x)  # in: Bx3xHxW -  out: Bx64xHxW
        x0 = self.input2(x0)  # in: Bx64xHxW - out: Bx64xHxW

        x1 = self.down1(x)  # in: Bx64xHxW -        out: Bx64xH/2xW/2
        x2 = self.down2(x1)  # in: Bx64xH/2xW/2 -    out: Bx64xH/4xW/4
        x3 = self.down3(x2)  # in: Bx64xH/4xW/4 -    out: Bx128xH/8xW/8
        x4 = self.down4(x3)  # in: Bx128xH/8xW/8 -   out: Bx256xH/16xW/16
        x5 = self.down5(x4)  # in: Bx256xH/16xW/16 - out: Bx512xH/32xW/32

        x = self.up4(x5, x4)  # in: Bx512xH/32xW/32, Bx256xH/16xW/16  - out: Bx512xH/16xW/16
        x = self.up3(x, x3)  # in: Bx512xH/16xW/16, Bx128xH/8xW/8    - out: Bx256xH/8xW/8
        x = self.up2(x, x2)  # in: Bx256xH/8xW/8,   Bx64xH/4xW/4     - out: Bx256xH/4xW/4
        x = self.up1(x, x1)  # in: Bx256xH/4xW/4,   Bx64xH/2xW/2     - out: Bx64xH/2xW/2
        x = self.up0(x, x0)  # in: Bx64xH/2xW/2,    Bx64xHxW         - out: Bx64xHxW

        out = self.output(x)  # in: Bx64xHxW - out: Bx1xHxW
        out = self.min_depth + torch.sigmoid(out) * (self.max_depth - self.min_depth)
        return out
        # return 0.3 * torch.sigmoid(out)


def main():
    model = ResNetUNet(n_channels=3, n_classes=1, bilinear=False).to(device)
    summary(model, input_size=(3, 256, 256))


if __name__ == '__main__':
    main()
