## DenseNet

## Result on DenseNet
```
HEIGHT = 256
WIDTH = 256
LR = 0.0002
EPOCHS = 10
BATCH_SIZE = 32
optimizer = Adam
DATASET = DIODE - Indoor - Validation
UP_SAMPLING_BLOCK = Conv2DTranspose
```

![](data/result.png)
***
