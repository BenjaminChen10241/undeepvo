import collections
import os

import numpy as np
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, ConcatDataset
from torch.utils.data import Dataset
from PIL import Image


class KittiLoader(Dataset):
    def __init__(self, root_dir, mode, transform=None):
        left_dir = os.path.join(root_dir, 'image_02/data/')
        self.left_paths = sorted([os.path.join(left_dir, fname) for fname in os.listdir(left_dir)])
        if mode == 'train':
            right_dir = os.path.join(root_dir, 'image_03/data/')
            self.right_paths = sorted([os.path.join(right_dir, fname) for fname in os.listdir(right_dir)])
            assert len(self.right_paths) == len(self.left_paths)
        self.transform = transform
        self.mode = mode

    def __len__(self):
        return len(self.left_paths) - 1

    def __getitem__(self, idx):
        left_image = Image.open(self.left_paths[idx])

        next_left_image = Image.open(self.left_paths[idx + 1])
        next_left_path = self.left_paths[idx + 1]

        if self.mode == 'train':
            right_image = Image.open(self.right_paths[idx])

            next_right_image = Image.open(self.right_paths[idx + 1])
            next_right_path = self.right_paths[idx + 1]

            sample = {
                'left_path': self.left_paths[idx],
                'right_path': self.right_paths[idx],
                'next_left_path': next_left_path,
                'next_right_path': next_right_path,
                'left_image': left_image,
                'right_image': right_image,
                'next_left_image': next_left_image,
                'next_right_image': next_right_image
            }

            if self.transform:
                sample = self.transform(sample)
                return sample
            else:
                return sample
        else:
            if self.transform:
                left_image = self.transform(left_image)
            return left_image


def image_transforms(mode='train', augment_parameters=[0.8, 1.2, 0.5, 2.0, 0.8, 1.2],
                     do_augmentation=True, transformations=None, size=(256, 512)):
    if mode == 'train':
        data_transform = transforms.Compose([
            ResizeImage(train=True, size=size),
            RandomFlip(do_augmentation),
            ToTensor(train=True),
            AugmentImagePair(augment_parameters, do_augmentation)
        ])
        return data_transform
    elif mode == 'test':
        data_transform = transforms.Compose([
            ResizeImage(train=False, size=size),
            ToTensor(train=False),
            DoTest(),
        ])
        return data_transform
    elif mode == 'custom':
        data_transform = transforms.Compose(transformations)
        return data_transform
    else:
        print('Wrong mode')


class ResizeImage(object):
    def __init__(self, train=True, size=(256, 512)):
        self.train = train
        self.transform = transforms.Resize(size)

    def __call__(self, sample):
        if self.train:
            new_right_image = self.transform(sample['right_image'])
            new_left_image = self.transform(sample['left_image'])
            new_next_right_image = self.transform(sample['next_right_image'])
            new_next_left_image = self.transform(sample['next_left_image'])

            sample = {
                'left_path': sample['left_path'],
                'right_path': sample['right_path'],
                'next_left_path': sample['next_left_path'],
                'next_right_path': sample['next_right_path'],
                'left_image': new_left_image,
                'right_image': new_right_image,
                'next_left_image': new_next_left_image,
                'next_right_image': new_next_right_image
            }

        else:
            left_image = sample
            new_left_image = self.transform(left_image)
            sample = new_left_image
        return sample


class DoTest(object):
    def __call__(self, sample):
        new_sample = torch.stack((sample, torch.flip(sample, [2])))
        return new_sample


class ToTensor(object):
    def __init__(self, train):
        self.train = train
        self.transform = transforms.ToTensor()

    def __call__(self, sample):
        if self.train:
            new_left_image = self.transform(sample['left_image'])
            new_right_image = self.transform(sample['right_image'])
            new_next_left_image = self.transform(sample['next_left_image'])
            new_next_right_image = self.transform(sample['next_right_image'])
            sample = {
                'left_path': sample['left_path'],
                'right_path': sample['right_path'],
                'next_left_path': sample['next_left_path'],
                'next_right_path': sample['next_right_path'],
                'left_image': new_left_image,
                'right_image': new_right_image,
                'next_left_image': new_next_left_image,
                'next_right_image': new_next_right_image
            }

        else:
            left_image = sample
            sample = self.transform(left_image)
        return sample


class RandomFlip(object):
    def __init__(self, do_augmentation):
        self.transform = transforms.RandomHorizontalFlip(p=1)
        self.do_augmentation = do_augmentation

    def __call__(self, sample):
        k = np.random.uniform(0, 1, 1)
        if self.do_augmentation:
            if k > 0.5:
                flipped_left = self.transform(sample['right_image'])
                flipped_right = self.transform(sample['left_image'])
                flipped_next_left_image = self.transform(sample['next_left_image'])
                flipped_next_right_image = self.transform(sample['next_right_image'])

                sample = {
                    'left_path': sample['left_path'],
                    'right_path': sample['right_path'],
                    'next_left_path': sample['next_left_path'],
                    'next_right_path': sample['next_right_path'],
                    'left_image': flipped_left,
                    'right_image': flipped_right,
                    'next_left_image': flipped_next_left_image,
                    'next_right_image': flipped_next_right_image
                }
        else:
            sample = {
                'left_path': sample['left_path'],
                'right_path': sample['right_path'],
                'next_left_path': sample['next_left_path'],
                'next_right_path': sample['next_right_path'],
                'left_image': sample['left_image'],
                'right_image': sample['right_image'],
                'next_left_image': sample['next_left_image'],
                'next_right_image': sample['next_right_image'],
            }
        return sample


class AugmentImagePair(object):
    def __init__(self, augment_parameters, do_augmentation):
        self.do_augmentation = do_augmentation
        self.gamma_low = augment_parameters[0]  # 0.8
        self.gamma_high = augment_parameters[1]  # 1.2
        self.brightness_low = augment_parameters[2]  # 0.5
        self.brightness_high = augment_parameters[3]  # 2.0
        self.color_low = augment_parameters[4]  # 0.8
        self.color_high = augment_parameters[5]  # 1.2

    def __call__(self, sample):
        left_image = sample['left_image']
        right_image = sample['right_image']
        next_left_image = sample['next_left_image']
        next_right_image = sample['next_right_image']

        p = np.random.uniform(0, 1, 1)
        if self.do_augmentation:
            if p > 0.5:
                # randomly shift gamma
                random_gamma = np.random.uniform(self.gamma_low, self.gamma_high)
                left_image_aug = left_image ** random_gamma
                right_image_aug = right_image ** random_gamma
                next_left_image_aug = next_left_image ** random_gamma
                next_right_image_aug = next_right_image ** random_gamma

                # randomly shift brightness
                random_brightness = np.random.uniform(self.brightness_low, self.brightness_high)
                left_image_aug = left_image_aug * random_brightness
                right_image_aug = right_image_aug * random_brightness
                next_left_image_aug = next_left_image_aug * random_brightness
                next_right_image_aug = next_right_image_aug * random_brightness

                # randomly shift color
                random_colors = np.random.uniform(self.color_low, self.color_high, 3)
                for i in range(3):
                    left_image_aug[i, :, :] *= random_colors[i]
                    right_image_aug[i, :, :] *= random_colors[i]
                    next_left_image_aug[i, :, :] *= random_colors[i]
                    next_right_image_aug[i, :, :] *= random_colors[i]

                # saturate
                left_image_aug = torch.clamp(left_image_aug, 0, 1)
                right_image_aug = torch.clamp(right_image_aug, 0, 1)
                next_left_image_aug = torch.clamp(next_left_image_aug, 0, 1)
                next_right_image_aug = torch.clamp(next_right_image_aug, 0, 1)

                sample = {
                    'left_path': sample['left_path'],
                    'right_path': sample['right_path'],
                    'next_left_path': sample['next_left_path'],
                    'next_right_path': sample['next_right_path'],
                    'left_image': left_image_aug,
                    'right_image': right_image_aug,
                    'next_left_image': next_left_image_aug,
                    'next_right_image': next_right_image_aug
                }

        else:
            sample = {
                'left_path': sample['left_path'],
                'right_path': sample['right_path'],
                'next_left_path': sample['next_left_path'],
                'next_right_path': sample['next_right_path'],
                'left_image': sample['left_image'],
                'right_image': sample['right_image'],
                'next_left_image': sample['next_left_image'],
                'next_right_image': sample['next_right_image'],
            }
        return sample


def prepare_dataloader(data_directory, mode, augment_parameters,
                       do_augmentation, batch_size, size, num_workers):
    data_dirs = os.listdir(data_directory)
    data_transform = image_transforms(
        mode=mode,
        augment_parameters=augment_parameters,
        do_augmentation=do_augmentation,
        size=size)
    datasets = [KittiLoader(os.path.join(data_directory,
                                         data_dir), mode, transform=data_transform)
                for data_dir in data_dirs]
    dataset = ConcatDataset(datasets)
    n_img = len(dataset)
    print('Use a dataset with', n_img, 'images')
    if mode == 'train':
        loader = DataLoader(dataset, batch_size=batch_size,
                            shuffle=False, num_workers=num_workers,
                            pin_memory=True)
    else:
        loader = DataLoader(dataset, batch_size=batch_size,
                            shuffle=False, num_workers=num_workers,
                            pin_memory=True)
    return n_img, loader


def to_device(input, device):
    if torch.is_tensor(input):
        return input.to(device=device)
    elif isinstance(input, str):
        return input
    elif isinstance(input, collections.Mapping):
        return {k: to_device(sample, device=device) for k, sample in input.items()}
    elif isinstance(input, collections.Sequence):
        return [to_device(sample, device=device) for sample in input]
    else:
        raise TypeError(f"Input must contain tensor, dict or list, found {type(input)}")


"""
n_img, loader = prepare_dataloader(
    dict_parameters['data_dir'],
    dict_parameters['mode'],
    dict_parameters['augment_parameters'],
    dict_parameters['do_augmentation'],
    dict_parameters['batch_size'],
    (dict_parameters['input_height'], dict_parameters['input_width']),
    dict_parameters['num_workers'],
)
"""
