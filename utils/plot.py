import numpy as np
import matplotlib.pyplot as plt
import kornia


def data_to_vis(tensor):
    return kornia.tensor_to_image(tensor)


def data_to_vis_normalized(tensor):
    return kornia.tensor_to_image(tensor) / 255.


def plot_information(images, predicted_depth, losses):
    sequence_name = images['left_path'][0]

    # <---------- Fetch image tensor ---------->
    left_image = data_to_vis(images['left_image'])
    right_image = data_to_vis(images['right_image'])
    left_image_depth = data_to_vis(predicted_depth['left_image_depth'])
    right_image_depth = data_to_vis(predicted_depth['right_image_depth'])
    next_left_image = data_to_vis(images['next_left_image'])
    next_right_image = data_to_vis(images['next_right_image'])
    next_left_image_depth = data_to_vis(predicted_depth['next_left_image_depth'])
    next_right_image_depth = data_to_vis(predicted_depth['next_right_image_depth'])

    fig, axarr = plt.subplots(4, 2)
    fig.set_size_inches(10, 6)

    # <---------- Plot left image ---------->
    axarr[0, 0].get_xaxis().set_ticks([])
    axarr[0, 0].get_yaxis().set_ticks([])
    axarr[0, 0].text(.0, -10, images['left_path'])
    axarr[0, 0].set_title('Left image')
    axarr[0, 0].imshow(left_image)

    axarr[0, 1].get_xaxis().set_ticks([])
    axarr[0, 1].get_yaxis().set_ticks([])
    axarr[0, 1].set_title('Left predicted depth image')
    axarr[0, 1].imshow(np.clip(1.0 / left_image_depth, 0, 100) / 100, cmap="inferno")

    # <---------- Plot right image ---------->
    axarr[1, 0].get_xaxis().set_ticks([])
    axarr[1, 0].get_yaxis().set_ticks([])
    axarr[1, 0].text(.0, -10, images['right_path'])
    axarr[1, 0].set_title('Right image')
    axarr[1, 0].imshow(right_image)

    axarr[1, 1].get_xaxis().set_ticks([])
    axarr[1, 1].get_yaxis().set_ticks([])
    axarr[1, 1].set_title('Right predicted depth image')
    axarr[1, 1].imshow(np.clip(1.0 / right_image_depth, 0, 100) / 100, cmap="inferno")

    # <---------- Plot next left image ---------->
    axarr[2, 0].get_xaxis().set_ticks([])
    axarr[2, 0].get_yaxis().set_ticks([])
    axarr[2, 0].text(.0, -10, images['next_left_path'])
    axarr[2, 0].set_title('Next left image')
    axarr[2, 0].imshow(next_left_image)

    axarr[2, 1].get_xaxis().set_ticks([])
    axarr[2, 1].get_yaxis().set_ticks([])
    axarr[2, 1].set_title('Next left predicted depth image')
    axarr[2, 1].imshow(np.clip(1.0 / next_left_image_depth, 0, 100) / 100, cmap="inferno")

    # <---------- Plot next left image ---------->
    axarr[3, 0].get_xaxis().set_ticks([])
    axarr[3, 0].get_yaxis().set_ticks([])
    axarr[3, 0].text(.0, -10, images['next_right_path'])
    axarr[3, 0].set_title('Next right image')
    axarr[3, 0].imshow(next_right_image)

    axarr[3, 1].get_xaxis().set_ticks([])
    axarr[3, 1].get_yaxis().set_ticks([])
    axarr[3, 1].set_title('Next right predicted depth image')
    axarr[3, 1].imshow(np.clip(1.0 / next_right_image_depth, 0, 100) / 100, cmap="inferno")

    fig.tight_layout()
    plt.subplots_adjust(left=0.0, right=0.95, top=0.95, bottom=0.162, wspace=0.452, hspace=0.243)
    plt.text(-845, 160, f"logs/Spatial Disparity Consistency : {losses['spatial_loss_value']}")
    plt.text(-845, 220, f"logs/Temporal loss : {losses['temporal_loss_value']}")
    plt.savefig('loss_information.png', bbox_inches='tight', dpi=200)
    # plt.show()
    plt.close()


def plot_image(image_tensor):
    image = data_to_vis(image_tensor)

    plt.imshow(image)
    plt.show()
    plt.close()


def plot_side_by_side(image_tensor_1, image_tensor_2):
    image_1 = data_to_vis(image_tensor_1)
    image_2 = data_to_vis(image_tensor_2)

    f, axarr = plt.subplots(2)
    axarr[0].imshow(image_1)
    axarr[1].imshow(image_2)
    plt.show()
    plt.close()


def plot_six_images(
        image_tensor_a_1, image_tensor_a_2, image_tensor_a_3,
        image_tensor_b_1, image_tensor_b_2, image_tensor_b_3,
        fig_name
):
    image_a_1 = data_to_vis(image_tensor_a_1)
    image_a_2 = data_to_vis(image_tensor_a_2)
    image_a_3 = data_to_vis(image_tensor_a_3)

    image_b_1 = data_to_vis(image_tensor_b_1)
    image_b_2 = data_to_vis(image_tensor_b_2)
    image_b_3 = data_to_vis(image_tensor_b_3)

    # fig, axarr = plt.subplots(2, 3, figsize=(80, 40))
    fig, axarr = plt.subplots(3, 2)
    axarr[0, 0].set_title('Right current image')
    axarr[0, 0].get_xaxis().set_ticks([])
    axarr[0, 0].get_yaxis().set_ticks([])
    axarr[0, 0].imshow(image_b_1)

    axarr[0, 1].set_title('Left current image')
    axarr[0, 1].get_xaxis().set_ticks([])
    axarr[0, 1].get_yaxis().set_ticks([])
    axarr[0, 1].imshow(image_a_1)

    axarr[1, 0].set_title('Predicted Right depth')
    axarr[1, 0].get_xaxis().set_ticks([])
    axarr[1, 0].get_yaxis().set_ticks([])
    axarr[1, 0].imshow(np.clip(1.0 / image_a_2, 0, 100) / 100, cmap="inferno")

    axarr[1, 1].set_title('Predicted Left depth')
    axarr[1, 1].get_xaxis().set_ticks([])
    axarr[1, 1].get_yaxis().set_ticks([])
    axarr[1, 1].imshow(np.clip(1.0 / image_b_2, 0, 100) / 100, cmap="inferno")

    axarr[2, 0].set_title('Synthesized Right image')
    axarr[2, 0].get_xaxis().set_ticks([])
    axarr[2, 0].get_yaxis().set_ticks([])
    axarr[2, 0].imshow(image_a_3)

    axarr[2, 1].set_title('Synthesized Left image')
    axarr[2, 1].get_xaxis().set_ticks([])
    axarr[2, 1].get_yaxis().set_ticks([])
    axarr[2, 1].imshow(image_b_3)

    fig.tight_layout()
    plt.subplots_adjust(left=0.02, right=0.98, top=0.95, bottom=0.162, wspace=0.1, hspace=0.243)

    # plt.show()
    plt.savefig(f'{fig_name}.png', bbox_inches='tight', dpi=200)
    plt.close()


def plot_four_image(im_tensor_1, im_tensor_2, im_tensor_3, im_tensor_4):
    f, axarr = plt.subplots(2, 2)
    axarr[0, 0].imshow(im_tensor_1)
    axarr[0, 1].imshow(im_tensor_2)
    axarr[1, 0].imshow(im_tensor_3)
    axarr[1, 1].imshow(im_tensor_4)


def visualize_depth_map(samples: tuple, test: bool = False, model=None) -> None:
    input, target = samples
    cmap = plt.cm.jet
    cmap.set_bad(color="black")

    assert input.shape[0] == target.shape[0]
    size = input.shape[0]

    fig, ax = plt.subplots(size, 2, figsize=(50, 50))
    for i in range(size):
        ax[i, 0].imshow((input[i].squeeze()))
        ax[i, 1].imshow((input[i].squeeze()), cmap=cmap)


def point_cloud_visualization(input: np.array, target: np.array) -> None:
    depth_vis = np.flipud(target.squeeze())
    img_vis = np.flipud(input.squeeze())

    fig = plt.figure(figsize=(15, 10))
    ax = plt.axes(projection="3d")

    STEP = 3
    for x in range(0, img_vis, shape[0], STEP):
        for y in range(0, img_vis.shape[1], STEP):
            ax.scatter(
                [depth_vis[x, y]] * 3,
                [y] * 3,
                [x] * 3,
                c=tuple(img_vis[x, y, :3] / 255),
                s=3
            )
        ax.view_init(45, 135)
