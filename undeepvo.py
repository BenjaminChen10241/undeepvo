import torch
from depth_estimator.resnet.model import ResNetUNet
from pose_estimator.resnet.model import PoseNet


class UnDeepVOTrain(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.depth_estimator = ResNetUNet(n_channels=3, n_classes=1)
        self.pose_net = PoseNet()

    def get_depth(self, input_image):
        return self.depth_estimator(input_image)

    def get_rot(self, x, ref_image):
        return self.pose_net(x, ref_image)

    def forward(self, input_image, ref_image):
        return self.get_depth(input_image), self.get_rot(input_image, ref_image)
