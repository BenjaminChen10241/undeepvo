import argparse
import torch
from torch import optim

from constants.constants import dict_parameters, TrainConstants, device, UnetParams, PoseNetParams
from losses.losses import UnDeepVoLoss
from undeepvo import UnDeepVOTrain
from utils.kitti_generator import prepare_dataloader, to_device
from utils.mlflow.mlflow_handler import TrainingProcessHandler
from utils.plot import plot_information


def main():
    parser = argparse.ArgumentParser(description='Run parameters')

    parser.add_argument('-main_dir',
                        default='dataset',
                        type=str,
                        dest='main_dir',
                        help='Path to the directory containing data')

    parser.add_argument('-split',
                        default=(100, 10, 10),
                        type=int,
                        dest='split',
                        nargs='+',
                        help='train/test/valid sequence split')

    parser.add_argument('-frames_range',
                        default=(0, 120, 1),
                        type=int,
                        dest='frames_range',
                        nargs='+',
                        help='frames_range from sequence')

    parser.add_argument('-epoch',
                        default=10,
                        type=int,
                        dest='epoch',
                        help='epochs to train')

    parser.add_argument('-batch_size',
                        default=2,
                        type=int,
                        dest='batch_size',
                        help='batch size')

    parser.add_argument('-model_path',
                        default="",
                        type=str,
                        help='whether to use resnet or not')

    args = parser.parse_args()

    # <-------------- Define model -------------->
    undeepvo_model: UnDeepVOTrain = UnDeepVOTrain()
    undeepvo_model.to(device)

    if args.model_path != "":
        undeepvo_model.load_state_dict(torch.load(args.model_path, map_location=args.device))

    undeepvo_model.train()

    # <-------------- Define loss function -------------->
    """
    spatial_losses = SpatialLosses(
        baseline=CameraParams.BASELINE,
        lambda_s=TrainConstants.SPATIAL_LAMBDA_S,
        focal_length=CameraParams.SCALED_FOCAL_LENGTH,
        trans_matrix=CameraParams.DEFAULT_TRANSFORMATION_MATRIX,
        left_camera_matrix=CameraParams.LEFT_CAMERA_MATRIX,
        right_camera_matrix=CameraParams.RIGHT_CAMERA_MATRIX,
        l_rotation=TrainConstants.SPATIAL_LAMBDA_ROTATION,
        l_translation=TrainConstants.SPATIAL_LAMBDA_TRANSLATION
    )
    temporal_losses = TemporalLosses(
        lambda_s=TrainConstants.TEMPORAL_LAMBDA_S,
        camera_matrix=CameraParams.RIGHT_CAMERA_MATRIX
    )
    """
    undeepvo_losses = UnDeepVoLoss()
    undeepvo_losses.to(device)

    # <-------------- Define optimizer -------------->
    optimizer = optim.Adam(
        undeepvo_model.parameters(),
        lr=TrainConstants.LR
    )

    n_img, loader = prepare_dataloader(
        dict_parameters['data_dir'],
        dict_parameters['mode'],
        dict_parameters['augment_parameters'],
        dict_parameters['do_augmentation'],
        dict_parameters['batch_size'],
        (dict_parameters['input_height'], dict_parameters['input_width']),
        dict_parameters['num_workers'],
    )

    handler = TrainingProcessHandler(
        enable_mlflow=True,
        mlflow_tags={"name": "mreza"},
        mlflow_parameters={
            "lr": TrainConstants.LR,
            "epoch": args.epoch,
            "temporal_lambda_s": TrainConstants.TEMPORAL_LAMBDA_S,
            "spatial_lambda_s": TrainConstants.SPATIAL_LAMBDA_S,
            "spatial_lambda_rotation": TrainConstants.SPATIAL_LAMBDA_ROTATION,
            "spatial_lambda_translation": TrainConstants.SPATIAL_LAMBDA_TRANSLATION,
            "max_depth": TrainConstants.MAX_DEPTH,
            "min_depth": TrainConstants.MIN_DEPTH,
            "batch_size": args.batch_size,
            "unet_width": UnetParams.WIDTH,
            "unet_height": UnetParams.HEIGHT,
            "unet_n_channels": UnetParams.N_CHANNELS,
            "unet_n_class": UnetParams.N_CLASSES,
            "posenet_width": PoseNetParams.WIDTH,
            "posenet_height": PoseNetParams.HEIGHT,
            "posenet_n_channels": PoseNetParams.N_CHANNEL
        },
        enable_iteration_progress_bar=True
    )

    handler.setup_handler(name="UnDeepVO", model=undeepvo_model)
    handler.start_callback(TrainConstants.EPOCHS, dict_parameters['batch_size'] * TrainConstants.EPOCHS)
    metrics = {}
    image_batches = None
    for epoch in range(TrainConstants.EPOCHS):
        for batch in loader:
            image_batches = batch
            batch = to_device(batch, device)
            left_image = batch['left_image']
            right_image = batch['right_image']
            left_path = batch['left_path']
            right_path = batch['right_path']
            next_left_image = batch['next_left_image']
            next_right_image = batch['next_right_image']
            next_left_path = batch['next_left_path']
            next_right_path = batch['next_right_path']

            undeepvo_model.zero_grad()

            left_depth, left_pose = undeepvo_model(
                input_image=left_image,
                ref_image=next_left_image
            )

            right_depth, right_pose = undeepvo_model(
                input_image=right_image,
                ref_image=next_right_image
            )

            next_left_depth, next_left_pose = undeepvo_model(
                input_image=next_left_image,
                ref_image=left_image
            )
            next_right_depth, next_right_pose = undeepvo_model(
                input_image=next_right_image,
                ref_image=right_image
            )

            losses = undeepvo_losses(
                left_current_image=left_image, right_current_image=right_image,
                left_current_depth=left_depth, right_current_depth=right_depth,
                left_next_image=next_left_image, right_next_image=next_right_image,
                left_next_depth=next_left_depth, right_next_depth=next_right_depth,
                left_current_rotation=left_pose[0], right_current_rotation=right_pose[0],
                left_next_rotation=next_left_pose[0], right_next_rotation=next_right_pose[0],
                left_current_translation=left_pose[1], right_current_translation=right_pose[1],
                left_next_translation=next_left_pose[1], right_next_translation=next_right_pose[1],
            )
            loss = losses['loss']
            loss.backward()

            optimizer.step()

            plot_information(
                images=batch,
                predicted_depth={
                    'left_image_depth': left_depth,
                    'right_image_depth': right_depth,
                    'next_left_image_depth': next_left_depth,
                    'next_right_image_depth': next_right_depth
                },
                losses={
                    'spatial_loss_value': undeepvo_losses.spatial_loss_value,
                    'temporal_loss_value': undeepvo_losses.temporal_loss_value
                }
            )

            metrics = {
                "loss": loss.item(),
                "inverse_depth_smoothness_loss": losses['spatial_loss'].item(),
                "pose_loss": losses['spatial_pose_consistency_loss'].item(),
                "spat_photo_loss": losses['spatial_photometric_consistency_loss'].item(),
                "disparity_loss": losses['spatial_disparity_consistency_loss'].item(),
                "temporal_loss": losses['temporal_loss'].item(),
                "registration_loss": losses['right_temporal_geometric_consistency_loss'].item()
            }
            handler.iteration_callback(metrics=metrics)
            print(f"########### {undeepvo_losses.spatial_loss_value} ###########")
            print(f"########### {undeepvo_losses.temporal_loss_value} ###########")

        handler.epoch_callback(metrics=metrics, image_batches=image_batches)

    handler.finish_callback(metrics=metrics)


if __name__ == '__main__':
    main()
