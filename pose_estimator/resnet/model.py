import torch
import torch.nn as nn
import torchvision.models

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class PoseNet(nn.Module):
    def __init__(self):
        super(PoseNet, self).__init__()

        self.first_layer = torch.nn.Conv2d(6, 64, kernel_size=7, padding=1, bias=False)

        # <--------- Base model --------->
        self.model = torchvision.models.resnet18(pretrained=True)
        self.model_layers = list(self.model.children())
        self.model = torch.nn.Sequential(*self.model_layers[1:-2])
        self.avgpool = torch.nn.AdaptiveAvgPool2d((6, 6))
        self.flatten = torch.nn.Flatten()

        # <--------- Rotation --------->
        self.x_rotation_conv1 = torch.nn.Linear(in_features=512 * 6 * 6, out_features=512)
        self.x_rotation_conv2 = torch.nn.Linear(in_features=512, out_features=512)
        self.x_rotation_output = torch.nn.Linear(in_features=512, out_features=3)

        # <--------- Translation --------->
        self.x_translation1 = torch.nn.Linear(in_features=512 * 6 * 6, out_features=512)
        self.x_translation2 = torch.nn.Linear(in_features=512, out_features=512)
        self.x_translation_output = torch.nn.Linear(in_features=512, out_features=3)

    def forward(self, x, reference_frame):
        """
        >> > x = torch.randn(2, 3)
        >> > x
        tensor([[0.6580, -1.0969, -0.4614],
                [-0.1034, -0.5790, 0.1497]])
        >> > torch.cat((x, x, x), 1)
        tensor([[0.6580, -1.0969, -0.4614, 0.6580, -1.0969, -0.4614, 0.6580,
                 -1.0969, -0.4614],
                [-0.1034, -0.5790, 0.1497, -0.1034, -0.5790, 0.1497, -0.1034,
                 -0.5790, 0.1497]])
        """
        x = torch.cat([x, reference_frame], dim=1)
        x = self.first_layer(x)
        x = self.model(x)
        x = self.avgpool(x)
        x = self.flatten(x)

        rotation = self.x_rotation_conv1(x)

        rotation = self.x_rotation_conv2(rotation)

        rotation = self.x_rotation_output(rotation)

        translation = self.x_translation1(x)

        translation = self.x_translation2(translation)

        translation = self.x_translation_output(translation)

        return rotation, translation
