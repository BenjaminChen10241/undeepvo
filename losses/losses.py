import torch

from constants.constants import CameraParams, TrainConstants, device
from losses.spatial import SpatialDisparityConsistencyLoss, SpatialPoseConsistencyLoss, \
    SpatialPhotometricConsistencyLoss
from losses.temporal import TemporalGeometricConsistencyLoss, TemporalPhotometricConsistencyLoss


class SpatialLosses(torch.nn.Module):
    def __init__(self, baseline, lambda_s, focal_length, trans_matrix, left_camera_matrix, right_camera_matrix,
                 l_rotation, l_translation):
        super(SpatialLosses, self).__init__()
        self.baseline = baseline
        self.lambda_s = lambda_s
        self.focal_length = focal_length
        self.trans_matrix = trans_matrix
        self.left_camera_matrix = left_camera_matrix
        self.right_camera_matrix = right_camera_matrix
        self.l_rotation = l_rotation
        self.l_translation = l_translation

        self.spatial_photometric_consistency_loss = SpatialPhotometricConsistencyLoss(
            baseline=self.baseline,
            lambda_s=self.lambda_s,
            focal_length=self.focal_length,
            trans_matrix=self.trans_matrix,
            left_camera_matrix=self.left_camera_matrix,
            right_camera_matrix=self.right_camera_matrix
        )

        self.spatial_disparity_consistency_loss = SpatialDisparityConsistencyLoss(
            baseline=self.baseline,
            right_camera_matrix=self.right_camera_matrix,
            left_camera_matrix=self.left_camera_matrix,
            trans_matrix=self.trans_matrix,
            focal_length=self.focal_length
        )

        self.spatial_pose_consistency_loss = SpatialPoseConsistencyLoss(
            l_rotation=self.l_rotation,
            l_translation=self.l_translation
        )

        self.spatial_disparity_consistency_loss.to(device)
        self.spatial_photometric_consistency_loss.to(device)
        self.spatial_pose_consistency_loss.to(device)

    def forward(self,
                left_current_image, right_current_image,
                left_current_depth, right_current_depth,
                left_rotation, left_translation,
                right_rotation, right_translation
                ):
        spatial_photometric_consistency_loss = self.spatial_photometric_consistency_loss(
            left_current_image, right_current_image, left_current_depth, right_current_depth
        )
        spatial_disparity_consistency_loss = self.spatial_disparity_consistency_loss(
            left_current_depth, right_current_depth
        )
        spatial_pose_consistency_loss = self.spatial_pose_consistency_loss(
            left_translation, left_rotation, right_translation, right_rotation
        )

        loss_values = {
            'spatial_loss': spatial_pose_consistency_loss + spatial_photometric_consistency_loss + spatial_disparity_consistency_loss,
            'spatial_pose_consistency_loss': spatial_pose_consistency_loss,
            'spatial_photometric_consistency_loss': spatial_photometric_consistency_loss,
            'spatial_disparity_consistency_loss': spatial_disparity_consistency_loss,
        }

        return loss_values


class TemporalLosses(torch.nn.Module):
    def __init__(self, lambda_s, camera_matrix):
        super(TemporalLosses, self).__init__()

        self.camera_matrix = camera_matrix
        self.lambda_s = lambda_s

        self.temporal_photometric_consistency_loss = TemporalPhotometricConsistencyLoss(
            lambda_s=self.lambda_s,
            camera_matrix=self.camera_matrix
        )

        self.temporal_geometric_consistency_loss = TemporalGeometricConsistencyLoss(
            camera_matrix=self.camera_matrix
        )

        self.temporal_photometric_consistency_loss.to(device)
        self.temporal_geometric_consistency_loss.to(device)

    def forward(self,
                left_current_image, right_current_image,
                left_current_depth, right_current_depth,
                left_next_image, right_next_image,
                left_next_depth, right_next_depth,
                left_current_rotation, right_current_rotation,
                left_next_rotation, right_next_rotation,
                left_current_translation, right_current_translation,
                left_next_translation, right_next_translation,
                ):
        left_temporal_photometric_consistency_loss = self.temporal_photometric_consistency_loss(
            left_current_image, left_next_image, left_current_depth, left_next_depth,
            left_current_translation, left_current_rotation, left_next_translation, left_next_rotation
        )

        right_temporal_photometric_consistency_loss = self.temporal_photometric_consistency_loss(
            right_current_image, right_next_image, right_current_depth, right_next_depth,
            right_current_translation, right_current_rotation, right_next_translation, right_next_rotation
        )

        left_temporal_geometric_consistency_loss = self.temporal_geometric_consistency_loss(
            left_current_depth, left_next_depth, left_current_translation,
            left_current_rotation, left_next_translation, left_next_rotation
        )

        right_temporal_geometric_consistency_loss = self.temporal_geometric_consistency_loss(
            right_current_depth, right_next_depth, right_current_translation,
            right_current_rotation, right_next_translation, right_next_rotation
        )

        loss_values = {
            'temporal_loss': left_temporal_geometric_consistency_loss + left_temporal_photometric_consistency_loss + \
                             right_temporal_geometric_consistency_loss + right_temporal_photometric_consistency_loss,
            'left_temporal_photometric_consistency_loss': left_temporal_photometric_consistency_loss,
            'right_temporal_photometric_consistency_loss': right_temporal_photometric_consistency_loss,
            'left_temporal_geometric_consistency_loss': left_temporal_geometric_consistency_loss,
            'right_temporal_geometric_consistency_loss': right_temporal_geometric_consistency_loss
        }
        return loss_values


class UnDeepVoLoss(torch.nn.Module):
    def __init__(self, ):
        super(UnDeepVoLoss, self).__init__()

        self.spatial_losses = SpatialLosses(
            baseline=CameraParams.BASELINE,
            lambda_s=TrainConstants.SPATIAL_LAMBDA_S,
            focal_length=CameraParams.SCALED_FOCAL_LENGTH,
            trans_matrix=CameraParams.DEFAULT_TRANSFORMATION_MATRIX,
            left_camera_matrix=CameraParams.LEFT_CAMERA_MATRIX,
            right_camera_matrix=CameraParams.RIGHT_CAMERA_MATRIX,
            l_rotation=TrainConstants.SPATIAL_LAMBDA_ROTATION,
            l_translation=TrainConstants.SPATIAL_LAMBDA_TRANSLATION
        )
        self.spatial_losses.to(device)

        self.temporal_losses = TemporalLosses(
            lambda_s=TrainConstants.TEMPORAL_LAMBDA_S,
            camera_matrix=CameraParams.RIGHT_CAMERA_MATRIX
        )
        self.temporal_losses.to(device)

    def forward(self,
                left_current_image, right_current_image,
                left_current_depth, right_current_depth,
                left_next_image, right_next_image,
                left_next_depth, right_next_depth,
                left_current_rotation, right_current_rotation,
                left_next_rotation, right_next_rotation,
                left_current_translation, right_current_translation,
                left_next_translation, right_next_translation,
                ):
        spatial_loss_values = self.spatial_losses(
            left_current_image=left_current_image, right_current_image=right_current_image,
            left_current_depth=left_current_depth, right_current_depth=right_current_depth,
            left_rotation=left_current_rotation, left_translation=left_current_translation,
            right_rotation=right_current_rotation, right_translation=right_current_translation
        )

        temporal_loss_values = self.temporal_losses(
            left_current_image=left_current_image, right_current_image=right_current_image,
            left_current_depth=left_current_depth, right_current_depth=right_current_depth,
            left_next_image=left_next_image, right_next_image=right_next_image,
            left_next_depth=left_next_depth, right_next_depth=right_next_depth,
            left_current_rotation=left_current_rotation, right_current_rotation=right_current_rotation,
            left_next_rotation=left_next_rotation, right_next_rotation=right_next_rotation,
            left_current_translation=left_current_translation, right_current_translation=right_current_translation,
            left_next_translation=left_next_translation, right_next_translation=right_next_translation,
        )

        loss = spatial_loss_values['spatial_loss'] + temporal_loss_values['temporal_loss']

        return {
            **spatial_loss_values,
            **temporal_loss_values,
            'loss': loss
        }
