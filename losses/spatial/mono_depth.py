import kornia
import torch
import torch.nn as nn
import torch.nn.functional as F

from utils.plot import plot_six_images


class SpatialPhotometricConsistencyLoss(nn.Module):
    def __init__(self, lambda_s, baseline, focal_length):
        super().__init__()
        self.BF = baseline * focal_length
        self.lambda_s = lambda_s
        self.L1Loss = torch.nn.L1Loss()
        self.SSIMLoss = kornia.losses.SSIMLoss(window_size=5)

    def get_disparities(self, left_depth, right_depth):
        left_disparity = 0.3 * torch.sigmoid(self.BF / left_depth)
        right_disparity = 0.3 * torch.sigmoid(self.BF / right_depth)
        return left_disparity, right_disparity

    def apply_disparity(self, img, disp):
        batch_size, _, height, width = img.size()

        # Original coordinates of pixels
        x_base = torch.linspace(0, 1, width).repeat(batch_size, height, 1).type_as(img)
        y_base = torch.linspace(0, 1, height).repeat(batch_size, width, 1).transpose(1, 2).type_as(img)

        # Apply shift in X direction
        x_shifts = disp[:, 0, :, :]  # Disparity is passed in NCHW format with 1 channel
        flow_field = torch.stack((x_base + x_shifts, y_base), dim=3)

        # In grid_sample coordinates are assumed to be between -1 and 1
        output = F.grid_sample(
            img,
            2 * flow_field - 1,
            mode='bilinear',
            padding_mode='zeros',
            align_corners=True
        )

        return output

    def generate_image_left(self, img, disp):
        return self.apply_disparity(img, -disp)

    def generate_image_right(self, img, disp):
        return self.apply_disparity(img, disp)

    def SSIM(self, x, y):
        C1 = 0.01 ** 2
        C2 = 0.03 ** 2

        mu_x = nn.AvgPool2d(3, 1)(x)
        mu_y = nn.AvgPool2d(3, 1)(y)
        mu_x_mu_y = mu_x * mu_y
        mu_x_sq = mu_x.pow(2)
        mu_y_sq = mu_y.pow(2)

        sigma_x = nn.AvgPool2d(3, 1)(x * x) - mu_x_sq
        sigma_y = nn.AvgPool2d(3, 1)(y * y) - mu_y_sq
        sigma_xy = nn.AvgPool2d(3, 1)(x * y) - mu_x_mu_y

        SSIM_n = (2 * mu_x_mu_y + C1) * (2 * sigma_xy + C2)
        SSIM_d = (mu_x_sq + mu_y_sq + C1) * (sigma_x + sigma_y + C2)
        SSIM = SSIM_n / SSIM_d

        return torch.clamp((1 - SSIM) / 2, 0, 1)

    def forward(self, left, right, left_depth, right_depth):
        """
        Args:
            left_depth: Bx1xHxW
            right_depth: Bx1xHxW
            left: BxCxHxW
            right: BxCxHxW
        Return:
            (float): The loss
        """
        left_disparity, right_disparity = self.get_disparities(left_depth, right_depth)
        # Generate images
        synthesized_left = self.generate_image_left(right, left_disparity)
        synthesized_right = self.generate_image_left(left, right_disparity)

        # L1
        l1_left = self.L1Loss(synthesized_left, left)
        l1_right = self.L1Loss(synthesized_right, right)

        # SSIM
        ssim_left = self.SSIMLoss(synthesized_left, left)
        ssim_right = self.SSIMLoss(synthesized_right, right)

        right_image_loss = l1_right + ssim_right
        left_image_loss = l1_left + ssim_left

        plot_six_images(
            left, right_disparity, synthesized_left,
            right, left_disparity, synthesized_right,
            fig_name='SpatialPhotometricConsistencyLoss'
        )

        return right_image_loss + left_image_loss
