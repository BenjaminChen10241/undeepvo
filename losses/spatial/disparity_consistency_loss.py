import torch
import kornia

from utils.plot import plot_six_images


class SpatialDisparityConsistencyLoss(torch.nn.Module):
    def __init__(self, baseline, focal_length, left_camera_matrix, right_camera_matrix, trans_matrix):
        super().__init__()
        self.BF = baseline * focal_length
        self.left_camera_matrix = left_camera_matrix
        self.right_camera_matrix = right_camera_matrix
        self.transformation_matrix = trans_matrix
        self.L1Loss = torch.nn.L1Loss()

    def get_disparities(self, left_depth, right_depth):
        left_disparity = self.BF / left_depth
        right_disparity = self.BF / right_depth
        return left_disparity, right_disparity

    def synthesize_disparity_map(self, left_disparity, right_disparity, left_current_depth, right_current_depth):
        synthesized_right_disparity = kornia.geometry.warp_frame_depth(
            image_src=left_disparity,
            depth_dst=right_current_depth,
            src_trans_dst=self.transformation_matrix,
            camera_matrix=self.left_camera_matrix,
        )

        synthesized_left_disparity = kornia.geometry.warp_frame_depth(
            image_src=right_disparity,
            depth_dst=left_current_depth,
            src_trans_dst=torch.inverse(self.transformation_matrix),
            camera_matrix=self.right_camera_matrix,
        )

        plot_six_images(
            left_disparity, right_current_depth, synthesized_right_disparity,
            right_disparity, left_current_depth, synthesized_left_disparity,
            fig_name='SpatialDisparityConsistencyLoss'
        )

        return synthesized_right_disparity, synthesized_left_disparity

    def forward(self, left_depth, right_depth):
        left_gt_disparity, right_gt_disparity = self.get_disparities(left_depth, right_depth)
        left_pred_disparity, right_pred_disparity = self.synthesize_disparity_map(
            left_gt_disparity,
            right_gt_disparity,
            left_depth,
            right_depth
        )
        return self.L1Loss(left_gt_disparity, left_pred_disparity) + self.L1Loss(right_gt_disparity,
                                                                                 right_pred_disparity)
