import torch
import kornia
from utils.loss import construct_transformation_matrix
from utils.plot import plot_six_images


class TemporalPhotometricConsistencyLoss(torch.nn.Module):
    def __init__(self, camera_matrix, lambda_s):
        super().__init__()
        self.camera_matrix = camera_matrix
        self.lambda_s = lambda_s
        self.L1Loss = torch.nn.L1Loss()
        self.SSIMLoss = kornia.losses.SSIMLoss(window_size=11)

    def synthesize_next_image(self, current_image, next_depth, next_to_current_transformation):
        synthesized_next_image = kornia.geometry.warp_frame_depth(
            image_src=current_image,
            depth_dst=next_depth,
            src_trans_dst=next_to_current_transformation,
            camera_matrix=self.camera_matrix,
            normalize_points=True
        )
        return synthesized_next_image

    def synthesize_current_image(self, next_image, current_depth, current_to_next_transformation):
        synthesized_current_image = kornia.geometry.warp_frame_depth(
            image_src=next_image,
            depth_dst=current_depth,
            src_trans_dst=current_to_next_transformation,
            camera_matrix=self.camera_matrix,
            normalize_points=True
        )
        return synthesized_current_image

    def forward(self, current_image, next_image, current_depth, next_depth,
                current_position, current_angle, next_position, next_angle):
        next_to_current_transformation = construct_transformation_matrix(current_position, current_angle)
        current_to_next_transformation = construct_transformation_matrix(next_position, next_angle)

        synthesized_next_image = self.synthesize_next_image(current_image, next_depth,
                                                            next_to_current_transformation)
        synthesized_current_image = self.synthesize_current_image(next_image, current_depth,
                                                                  current_to_next_transformation)

        plot_six_images(
            current_image, next_depth, synthesized_next_image,
            next_image, current_depth, synthesized_current_image,
            fig_name="TemporalPhotometricConsistencyLoss"
        )

        current_loss = self.lambda_s * self.SSIMLoss(current_image, synthesized_current_image) + (
                1 - self.lambda_s) * self.L1Loss(current_image, synthesized_current_image)

        next_loss = self.lambda_s * self.SSIMLoss(next_image, synthesized_next_image) + (
                1 - self.lambda_s) * self.L1Loss(next_image, synthesized_next_image)

        return current_loss + next_loss
