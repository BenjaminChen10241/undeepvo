from .photometric_consistency_loss import TemporalPhotometricConsistencyLoss
from .geometric_registration_loss import TemporalGeometricConsistencyLoss
